package DZ2_1Dopp;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        final int max = 1000;
        final int rnd = rnd(max);

        game(choice, rnd);

    }
    public static void game(int num1, int num2) {
        Scanner scanner = new Scanner(System.in);
        while (num1 > 0){
            if (num1 > num2){
                System.out.println("Число больше загаданного системой");
                num1 = scanner.nextInt();
            }
            if (num1 == num2){
                System.out.println("Вы угадали");
                break;
            }
            else {
                System.out.println("Число меньше загаданного системой");
                num1 = scanner.nextInt();
            }
        }
    }


    public static int rnd(int max)
    {
        return (int) (Math.random() * ++max);
    }
}
