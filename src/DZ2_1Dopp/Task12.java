package DZ2_1Dopp;
import java.util.Scanner;
import java.util.Arrays;

public class Task12 {
    public static void main (String[]args){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
            arr1[i] = (int) Math.pow(arr[i], 2);
        }
        Arrays.sort(arr1);
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");

        }
    }
}

