package DZ3_1.task2;
import java.util.Arrays;

public class Student {
    private String name;
    private String surName;
    private static int[] grades;
    private double midlGrade;

    public Student(){

    }
    public Student(String name, String surName, double midlGrade){
        this.name = name;
        this.surName = surName;
        this.midlGrade = midlGrade;

    }
    public String getName(){
        return name;
    }
    public String getSurName(){
        return surName;
    }
    public double getMidlGrade(){ return midlGrade; }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurName(String surName) {
        this.surName = surName;
    }
    public void setMidlGrade(double midlGrade) { this.midlGrade = midlGrade; }
    public void getGrades() {
        System.out.println(Arrays.toString(grades));
    }
    public void setGrades(int[] grades) {
        this.grades = grades;
    }


    /**
     * Функция сдигает массив на один влево
     *
     * @param grades    инзначальный массив оценок
     * @param nextGrade новая оценка
     */
    public static void changeGrades(int[] grades, int nextGrade){
        int[] a  = Arrays.copyOf(Arrays.copyOfRange(grades, 1, grades.length), grades.length);
        a[a.length - 1] = nextGrade;
        Student.grades = a;

    }

    /**
     * Функция находит среднюю оценку из массива оценок студента
     * @param grades массив пареметров
     * @return средняя оценка
     */
    public static double midlGrade(int[] grades){
        double sum = 0;
        for (int grade : grades) {
            sum += grade;
        }
        return Math.floor(sum / grades.length * 100) / 100.0;
    }
    public void printStudents(){
        System.out.println(this.name + " " + this.surName + " " + this.midlGrade);
    }
}


