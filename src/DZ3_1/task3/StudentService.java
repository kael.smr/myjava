package DZ3_1.task3;

import DZ3_1.task2.Student;

import java.util.Arrays;
import java.util.Comparator;


public class StudentService {
    public static void bestStudent(Student[] students){

        double maxMidlGrade = 0;
        String name = null;
        String surName = null;
        for (Student student : students) {
            if (maxMidlGrade < student.getMidlGrade()){
                maxMidlGrade = student.getMidlGrade();
                name = student.getName();
                surName = student.getSurName();
            }
        }

        System.out.println(name + " " + surName + " " + maxMidlGrade);

    }
    public static void sortBySurname(Student[] students) {
        Arrays.stream(students).sorted(Comparator.comparing(Student::getSurName)).forEach(Student::printStudents);
    }
}

