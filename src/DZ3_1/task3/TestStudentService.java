package DZ3_1.task3;

import DZ3_1.task2.Student;




public class TestStudentService {
    public static void main(String[] args) {

        Student fedor = new Student("Федор", "Сумкин", 4.5);
        Student lev = new Student("Лев", "Жиглов", 4.2);
        Student vladimir = new Student("Владимир", "Шарапов", 4.0);
        Student denis = new Student("Денис", "Николаев", 4.7);
        Student aleks = new Student("Алексей", "Прокофьев", 4.9);

        Student[] students = new Student[5];
        students[0] = fedor;
        students[1] = lev;
        students[2] = vladimir;
        students[3] = denis;
        students[4] = aleks;
        StudentService.bestStudent(students);
        StudentService.sortBySurname(students);
    }
}
