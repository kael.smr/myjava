package DZ3_1.task4;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class TimeUnit {
    Scanner scanner = new Scanner(System.in);
    private final int hour;
    private final int minute;
    private final int seconds;

    public TimeUnit(int hour, int minute, int seconds){
        while (hour > 23){
            System.out.println("Значение часа не может быть выше 23");
            hour = scanner.nextInt();
        }
        this.hour = hour;
        while (minute > 59){
            System.out.println("Значение минут не может быть выше 59");
            minute = scanner.nextInt();
        }
        this.minute = minute;
        while (seconds > 59){
            System.out.println("Значение секунд не может быть выше 59");
            seconds = scanner.nextInt();
        }
        this.seconds = seconds;
    }
    public TimeUnit(int hour, int minute){
        while (hour > 23){
            System.out.println("Значение часа не может быть выше 23");
            hour = scanner.nextInt();
        }
        this.hour = hour;
        while (minute > 59){
            System.out.println("Значение минут не может быть выше 59");
            minute = scanner.nextInt();
        }
        this.minute = minute;
        this.seconds = 0;
    }
    public TimeUnit(int hour){
        while (hour > 23){
            System.out.println("Значение часа не может быть выше 23");
            hour = scanner.nextInt();
        }
        this.hour = hour;
        this.minute = 0;
        this.seconds = 0;
    }


    public void print(){
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR_OF_DAY, hour);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.SECOND, seconds);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        System.out.println(df.format(time.getTime()));

    }
    public void print1(){
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR_OF_DAY, hour);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.SECOND, seconds);
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss a");
        System.out.println(df.format(time.getTime()));

    }

    public void nextTime(int h, int m, int s){
        Calendar time = new GregorianCalendar();
        time.set(Calendar.HOUR_OF_DAY, hour + h);
        time.set(Calendar.MINUTE, minute + m);
        time.set(Calendar.SECOND, seconds + s);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        System.out.print(df.format(time.getTime()));

    }
}
