package DZ3_1.task5;

public class DayOfWeek {
    private byte num;
    private String day;


    public DayOfWeek(byte num, String day){
        this.num = num;
        this.day = day;
    }

    public byte getNum() {
        return num;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setNum(byte num) {
        this.num = num;
    }
    public static void printWeek(DayOfWeek[] dayOfWeeks){
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            System.out.println(dayOfWeek.getNum() + " " + dayOfWeek.getDay());

        }
    }
}
