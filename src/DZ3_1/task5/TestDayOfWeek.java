package DZ3_1.task5;

public class TestDayOfWeek {
    public static void main(String[] args) {
        DayOfWeek first = new DayOfWeek((byte) 1, "Monday");
        DayOfWeek second = new DayOfWeek((byte) 2, "Tuesday");
        DayOfWeek thirds = new DayOfWeek((byte) 3, "Wednesday");
        DayOfWeek fourth = new DayOfWeek((byte) 4, "Thursday");
        DayOfWeek fifth = new DayOfWeek((byte) 5, "Friday");
        DayOfWeek sixth = new DayOfWeek((byte) 6, "Saturday");
        DayOfWeek seventh = new DayOfWeek((byte) 7, "Sunday");
        DayOfWeek[] dayOfWeeks = new DayOfWeek[7];
        dayOfWeeks[0] = first;
        dayOfWeeks[1] = second;
        dayOfWeeks[2] = thirds;
        dayOfWeeks[3] = fourth;
        dayOfWeeks[4] = fifth;
        dayOfWeeks[5] = sixth;
        dayOfWeeks[6] = seventh;
        DayOfWeek.printWeek(dayOfWeeks);




    }
}
