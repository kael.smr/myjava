package DZ3_1.task6;



public class AmazingString {
    private char[] arr;



    public AmazingString(char[] arr){
        this.arr = arr;
    }
    public AmazingString(String str){
        char[] arr = new char[str.length()];
        for (int i = 0; str.length() > i; i++) {
            arr[i] = str.charAt(i);
        }
        this.arr = arr;
    }

    public char returnChar(String str, int x) {
        return str.charAt(x);
    }
    public int longSting(String str){
        return str.length();
    }
    public void printString(String str){
        System.out.println(str);
    }
    public boolean checkCharInString(char[] arr, String str){
        StringBuilder string = new StringBuilder();
        for (char c : arr) {
            string.append(c);
        }
        return str.contains(string.toString());
    }
    public void checkString(String str, String string){
        System.out.println(str.contains(string));
    }
    public String spaseIgnore(String str){
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != ' '){
                string.append(str.charAt(i));
            }
        }return string.toString();
    }
    public String reverseString(String str){
        StringBuilder string = new StringBuilder();
        for (int i = str.length(); i > 0; i--) {
            string.append(str.charAt(i - 1));

        }return string.toString();
    }







    public void setArr(char[] arr) {
        this.arr = arr;
    }

    public char[] getArr() {
        return arr;
    }



}
