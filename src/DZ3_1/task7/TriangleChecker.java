package DZ3_1.task7;

public class TriangleChecker {
    private double a;
    private double b;
    private double c;

    public TriangleChecker() {

    }

    public static boolean checker(double a, double b, double c) {
        boolean check = false;
        if (a + b > c) {
            if (a + c > a) {
                if (b + c > a) {
                    check = true;
                }
            }
        }
        return check;
    }
}
