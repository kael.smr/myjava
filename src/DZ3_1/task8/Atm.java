package DZ3_1.task8;

public class Atm {

    private double course;

    private static int operations = 0;

    public Atm(double course){
        this.course = course;
        ++operations;
    }



    public static void getOperations() {
        System.out.println(operations);
    }

    public double getCourse() {
        return course;
    }

    public static void dollarsPerRubles(double rubles, double course){
        System.out.println(rubles * course);
    }
    public static void rublesPerDollars(double dollars, double course){
        System.out.println(dollars / course);
    }
}

