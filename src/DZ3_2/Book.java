package DZ3_2;

public class Book  extends Library{  //Класс книга
    private String bookStatus;
    private String nameBook;
    private String bookAuthor;


    Book(String bookAuthor, String nameBook) { //Создаем новую книгу
        this.bookAuthor = bookAuthor;
        this.nameBook = nameBook;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public String getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(String bookStatus) {
        this.bookStatus = bookStatus;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }
}
