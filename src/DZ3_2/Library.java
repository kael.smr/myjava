package DZ3_2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {
    final String STATUS_BOOK_ON = "Есть в наличии";
    final String STATUS_BOOK_OFF = "На руках";
    private final List<Book> books = new ArrayList<>(); //Массив книг
    private final List<User> users = new ArrayList<>();//Массив зарегестрированных пользователей


    public void addBook(String bookAuthor, String nameBook) { //Добавляем книгу в библиотеку
        Book book = new Book(bookAuthor, nameBook);
        if ((books).size() == 0) {         // Если книг в библиотеке еше нет то добавляем
            book.setBookStatus(STATUS_BOOK_ON);  //Для добавленной книги уставнавливаем статус , что книга доступна
            books.add(book);
        } else {
            for (Book p : books) {
                if (p.getNameBook().equalsIgnoreCase(book.getNameBook())) { //Проверяем есть ли уже такая книга в библиотеке
                    System.out.println("Эта книга уже есть");
                    break;
                } else {
                    books.add(book);
                    book.setBookStatus(STATUS_BOOK_ON);
                    break;
                }
            }
        }
    }

    public void deleteBook(String nameBook) {// Удаляем книгу из библиотеки
        Iterator<Book> iterator = books.iterator();
        while (iterator.hasNext()) {
            Book book = iterator.next();
            if (book.getNameBook().equalsIgnoreCase(nameBook) && book.getBookStatus().equals(STATUS_BOOK_ON)) {
                iterator.remove(); // Удаляем книгу если она находиться в библиотеке и доступна для выдачи
            }
        }
    }

    public void searchBooksAuthor(String authorName) { // Ищем все книги автора которые есть в библиотеке
        for (Book book : books) {
            if (book.getBookAuthor().equalsIgnoreCase(authorName)) {
                System.out.println(book.getNameBook());
            }
        }
    }

    public Book getSearchBooks(String bookName) {  // Ищем книгу по названию и возвращаем ее
        Book searchBook = null;
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                searchBook = book;
            }

        }
        return searchBook;
    }

    public void newUser(String surname) { // Добавляем нового пользователя , и присваем ему персональный номер
        User user = new User( surname);
        users.add(user);
        System.out.println("Ваш персональный номер: " + user.getUserId());
    }

    public void newUser( String surname, String bookName) { //Добавляем нового пользователя , и можем ему передать книгу
        User user = new User( surname);    // Если она доступна
        users.add(user);   //Добавляем пользователя в библиотек , вне зависимости от результата
        for (Book book : books) {
            if (book.getNameBook().equalsIgnoreCase(bookName)) {
                if (book.getBookStatus().equals(STATUS_BOOK_ON)) {
                    book.setBookStatus(STATUS_BOOK_OFF);
                    user.setBookUserTake(book.getNameBook());
                    System.out.println("Ваш персональный номер: " + user.getUserId());
                } else {
                    System.out.println("Эта книга занята. " + "Вам присвоен персональный номер: " + user.getUserId());
                }
            }
        }
    }

    public void userTakeBook(int userId, String bookName) { //Читатель берет книгу , по своему персональному номеру
        if (users.get(userId).getBookUserTake() == null) { //Проверям есть ли у мользователя книга
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    if (book.getBookStatus().equals(STATUS_BOOK_ON)) { //Если книга доступна для выдачи
                        users.get(userId).setBookUserTake(book.getNameBook()); //Читателю присваем название книги
                        book.setBookStatus(STATUS_BOOK_OFF);  // У книги меняем статус
                    } else {
                        System.out.println("Книгу уже взяли");
                        break;
                    }
                }
            }
        } else
            System.out.println("У вас уже есть книга.");
    }

    public void returnBookInLibrary(int userId, String bookName) { // Читатель возвращает книгу в библиотеку
        if (users.get(userId).getBookUserTake().equalsIgnoreCase(bookName)) { //Смотрим есть ли у читателя эта книга
            users.get(userId).setBookUserTake(null); //Устанавливаем значение null для читателя
            for (Book book : books) {
                if (book.getNameBook().equalsIgnoreCase(bookName)) {
                    book.setBookStatus(STATUS_BOOK_ON);//Книга снова доступна для выдачи
                }

            }
        } else {
            System.out.println("Вы не брали эту книгу.");
        }
    }


    // Методы для теста , верно ли отображаються значения
    public void statusBook() {
        for (Book book : books) {
            System.out.println(book.getBookAuthor() + " " + book.getNameBook() + " " + book.getBookStatus());

        }

    }

    public void statusPerson() {
        for (User user : users) {
            System.out.println(user.getSurname() + " " + user.getUserId() + " " + user.getBookUserTake());

        }

    }
}




