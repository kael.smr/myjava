package DZ3_2;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        library.addBook("Пушкин", "Сборник стихотворений");
        library.addBook("Пехов", "Въюга теней");
        library.addBook("Пехов", "Под знаком мантикоры");
        library.addBook("Сургалинов", "Дисгардиум");
        library.addBook("Ефремов", "Посмертие");
        library.addBook("Куропятник", "Игра миров");
        library.statusBook();
        System.out.println();

        library.deleteBook("Дисгардиум");
        library.statusBook();
        System.out.println();

        library.searchBooksAuthor("Пехов");
        System.out.println();

        library.newUser( "Иванов");
        library.newUser( "Петров");
        library.newUser( "Сидоров");
        library.newUser("Ветров", "Под знаком мантикоры");
        library.newUser( "Иванов", "Въюга теней");

        library.userTakeBook(0, "Посмертие");
        library.userTakeBook(2, "Игра миров");
        library.userTakeBook(3, "Посмертие");
        library.userTakeBook(3, "Игра миров");

        library.returnBookInLibrary(3, "Игра миров");
        library.userTakeBook(3, "Игра миров");

        library.userTakeBook(3, "Сборник стихотворений");
        System.out.println();
        library.statusPerson();
        System.out.println();
        library.statusBook();

    }
}
