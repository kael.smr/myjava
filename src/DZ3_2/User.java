package DZ3_2;

public class User extends Library {
    private static int allId;  // Счетчик количества пользователей библиотеки
    private int userId;    //id пользователя
    private  String surname;
    private String bookUserTake; //Название книги которую взял читатель
    User(String surname){
        this.surname = surname;
        this.userId = allId;
        allId++;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public int getUserId() {
        return userId;
    }

    public void setBookUserTake(String bookUserTake) {
        this.bookUserTake = bookUserTake;
    }

    public String getBookUserTake() {
        return bookUserTake;
    }
}
