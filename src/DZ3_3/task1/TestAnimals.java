package DZ3_3.task1;

public class TestAnimals {
    public static void main(String[] args) {
        Animals dolphin = new Dolphin();
        Animals eagle = new Eagle();
        eagle.eat();
        dolphin.sleep();
        Animals goldFish = new Fish() {
            @Override
            public void move() {

            }
        };
        goldFish.sleep();
        goldFish.eat();
        eagle.wayOfBirth();
    }

}
