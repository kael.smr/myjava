package DZBasicModul.DZ3_3.task1;

public class Bat extends Animals implements Move,WayOfBirth{
    @Override
    public void eat() {
        System.out.println("Питание");
    }

    @Override
    public void sleep() {
        System.out.println("Сон");
    }

    @Override
    public void move() {
        System.out.println("Летает медленно");
    }

    @Override
    public void wayOfBirth() {
        System.out.println("Живородящие");
    }
}
