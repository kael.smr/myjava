package DZBasicModul.DZ3_3.task1;

public class Egle extends Animals implements Move,WayOfBirth{
    @Override
    public void eat() {
        System.out.println("Питание");
    }

    @Override
    public void sleep() {
        System.out.println("Сон");
    }

    @Override
    public void move() {
        System.out.println("Летает быстро");
    }

    @Override
    public void wayOfBirth() {
        System.out.println("Откладывают яйца");
    }
}
