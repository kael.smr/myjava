package DZBasicModul.DZ3_3.task1;

public class Main {
    public static void main(String[] args) {
        Animals bat = new Bat();
        Animals egle = new Egle();
        Animals dolphin = new Dolphin();
        Animals goldFish = new GoldFish();
        bat.eat();
        bat.sleep();
        bat.move();
        bat.wayOfBirth();
        egle.eat();
        egle.sleep();
        egle.move();
        egle.wayOfBirth();
        dolphin.eat();
        dolphin.sleep();
        dolphin.move();
        dolphin.wayOfBirth();
        goldFish.eat();
        goldFish.sleep();
        goldFish.move();
        goldFish.wayOfBirth();

    }
}
