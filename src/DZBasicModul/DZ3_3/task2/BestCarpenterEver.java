package DZBasicModul.DZ3_3.task2;

public class BestCarpenterEver implements Repair{
    private final String nameOfFurniture;

    public BestCarpenterEver(String nameOfFurniture){
        this.nameOfFurniture = nameOfFurniture;
    }

    @Override
    public boolean repair() {
        return nameOfFurniture.equalsIgnoreCase("Табуретки");

    }
}
