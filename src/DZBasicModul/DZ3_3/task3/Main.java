package DZBasicModul.DZ3_3.task3;


import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
        ArrayList<Integer> list;

        for (int i = 0; i < x; i++) {
            list = new ArrayList<>();
            for (int j = 0; j < y; j++) {
                list.add(i + j);
            }
            matrix.add(list);
        }
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                System.out.print(matrix.get(j).get(i) + " ");
            }
            System.out.println();
        }


    }
}
