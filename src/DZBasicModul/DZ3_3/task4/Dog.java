package DZBasicModul.DZ3_3.task4;

public class Dog {
    private final String dogName;

    public Dog(String name){
        this.dogName = name;
    }

    public String getName() {
        return dogName;
    }
}
