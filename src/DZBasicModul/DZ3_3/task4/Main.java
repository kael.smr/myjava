package DZBasicModul.DZ3_3.task4;

/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
 */


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    private static int n;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите общее количество участников:");
        n = scanner.nextInt();
        System.out.println("Введите имена хозяев:");
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            names.add(scanner.next());
        }
        System.out.println("Введите клички питомцев:");
        ArrayList<Dog> dogs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            dogs.add(new Dog(scanner.next()));
        }
        System.out.println("Введите оценки судей:");
        ArrayList<Double> rating = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            rating.add((int) ((scanner.nextDouble() + scanner.nextDouble() + scanner.nextDouble()) / 3) * 10 / 10.0);
        }
        result(names, dogs, rating);


    }
    public static void result( ArrayList<String> names, ArrayList<Dog> dogs, ArrayList<Double> rating) {
        ArrayList<Participant> participants = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            participants.add(new Participant(names.get(i), dogs.get(i).getName(), rating.get(i)));
        }
        participants.sort(Comparator.comparingDouble(Participant::getRating).reversed());
        for (int i = 0; i < 3; i++) {
            System.out.println(participants.get(i).getNamePerson() + " " + participants.get(i).getNameDog() + " " + participants.get(i).getRating());

        }
    }
}
