package DZBasicModul.DZ3_3.task4;

public class Participant {
    private final String namePerson;
    private final String nameDog;
    private final double rating;

    public Participant(String namePerson, String nameDog, double rating){
        this.namePerson = namePerson;
        this.nameDog = nameDog;
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public String getNameDog() {
        return nameDog;
    }

    public String getNamePerson() {
        return namePerson;
    }
}
