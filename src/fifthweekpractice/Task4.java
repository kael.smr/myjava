package fifthweekpractice;
/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7
    */

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int[] arr1 = new int[n];
        for (int i = 0; i < arr1.length ; i++) {
            arr1[i] = input.nextInt();

        }
        int k = input.nextInt();
        int[] arr2 = new int[k];
        for (int i = 0; i < k ; i++) {
            arr2[i] = input.nextInt();

        }
        mergeTwoArrayWithLoop(arr1, arr2);
        mergeToArraysWithSystemArrayCopy(arr1, arr2);
        mergeTwoArrays(arr1,arr2);

    }

    /**
     * Метод делает слияние двух отсортированных массивов в третий результирующий
     * @param arr1 первый массив
     * @param arr2 второй массив
     */

    public static void mergeTwoArrayWithLoop (int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];

        int pos = 0;
        //копируем элементы первого массива в наш результирующий
        for (int element : arr1) {
            mergeArray[pos] = element;
            pos++;
        }
        //копируем элементы второго массива в наш результирующий
        for (int element : arr2) {
            mergeArray[pos] = element;
            pos++;
        }
        //сортируем
        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }
    public static void mergeToArraysWithSystemArrayCopy(int[] arr1, int[] arr2) {
        int [] mergeArray = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, mergeArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergeArray, arr1.length, arr2.length);
        System.out.println(Arrays.toString(mergeArray));
    }
    public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            } else {
                mergedArray[k++] = arr2[j++];
            }
        }
            // mergeArray[k++] = arr1[i] < arr2[j] ?

            //сохраняем оставшиеся элементы первого массива
            while (i < arr1.length) {
                mergedArray[k++] = arr1[i++];
            }

            //сохраняем оставшиеся элементы второго массива
            while (j < arr2.length) {
                mergedArray[k++] = arr2[j++];
            }
            System.out.println(Arrays.toString(mergedArray));
        }
    }



