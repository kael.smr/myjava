package firstweekpractice;

import java.util.Scanner;

/* Напишите программу, которая получает два числа с плавающей точкой x и y в аргументах командной строки и выводит
евклидовое растояние от точки (x,y) до точки (0,0)

Входные данные:
i = 7 j = 5
Евклидово расстояние - это расстояние между двумя точками, вычисляемое по теореме Пифагора.
(сумма квадратов длин катетов равна квадрату длины гипотенузы.)
корень квадратный из суммы квадратов x2-x1 и y2-y1
d = sqrt((x2-x1)^2 + (y2-y1)^2)
    y
    |
    |
 4  |______(3,4)
    |      /|
    |    /  |
    |  /    |
    |/______|_____________x
    0       3
 */
public class Task3 {
    public static void main(String[] args) {
        double d;
        double y2 = 0;
        double x2 = 0;
        Scanner scanner = new Scanner(System.in);
        double x1 = scanner.nextDouble();
        double y1 = scanner.nextDouble();

        d = Math.sqrt((Math.pow((x2 - x1), 2)) + (Math.pow((y2 - y1), 2)));
        System.out.println("Расстояние равно: " + d);


    }
}
