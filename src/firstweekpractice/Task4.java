package firstweekpractice;

import java.util.Scanner;

/*
Дана площадь круга, нам нужно найти диаметр окружности и длину окружности.
S = Pi * (D^2 / 4) - это через диаметр => d = sqrt(S * 3 / Pi)
S = Pi * R^2 - радиус
S = L^2 / (4 * Pi) - площадь через длину
Отношение длины к диаметру окружности является постоянным числом.
Pi = L / d

Входные данные:
S = 91

 */
public class Task4 {
    public static void main(String[] args) {
//        final double Pi = 3.14;
//        final int S = 91;
//        double L, R, R2, D, L2;
//
//        R2 = Pi / S;
//        System.out.println("Квадрат радиуса: " + R2);
//
//        R = Math.sqrt(R2);
//        System.out.println("Радиус: " + R);
//
//        D = R * 2;
//        System.out.println("Диаметр: " + D);
//
//        L2 = S * (Pi * 4);
//        L = Math.sqrt(L2);
//        System.out.println("Длина окружности: " + L);
        Scanner scanner = new Scanner(System.in);
        int square = scanner.nextInt();
        //диаметр
        double d = Math.sqrt(square * 4 / Math.PI);
        double l = Math.PI * d;

        System.out.println("Результат: d=" + d + "; l=" + l);




    }
}
