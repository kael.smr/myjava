package firstweekpractice;

import java.util.Scanner;

/*
    Перевод литров в галлоны. С консоли считывается число n - количество литров, нужно перевести его в галлоны.
    (1 литр = 0,219969 галлона)
 */
public class Task7 {
    public static final double LITERS_IN_GALLON = 0.219969;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int litersAll = scanner.nextInt();
        // double res = littersAll + LITERS_IN_GALLON;
        //System.out.println("Результат: " + res);

        System.out.println(litersAll + " литров это " + (litersAll * LITERS_IN_GALLON) + " галлонов");

    }
}
