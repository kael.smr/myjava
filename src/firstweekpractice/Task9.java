package firstweekpractice;
/*
Даны целые числа a, b, c, определяющие квадратное уравнение. Вычислить дискриминант.
Подсказка: D = b^2 - 4 * a * c
Входные данные:
a = 6
b = -28
c = 79
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        double res;
        res = Math.pow(b, 2) - 4 * a * c;
        System.out.println("Результат: " + res);

    }
}
