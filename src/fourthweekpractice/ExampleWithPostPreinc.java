package fourthweekpractice;

public class ExampleWithPostPreinc {
/*
Примеры на пре-пост инкремент
 */
public static void main(String[] args) {
//    //post-inc
//    int i = 1;
//    int a = i++; //сначала присваивается значение, потом выполняется изменение переменной
//
//    int temp = i;
//    i = i + 1;
//    a = temp;
//    System.out.println(i);
//    System.out.println(a);
    //pre-inc
    int i = 1;
    int a = ++i; // сначала выполняется изменение переменной i потом присваивается значение a
    System.out.println(i);
    System.out.println(a);
}
}
