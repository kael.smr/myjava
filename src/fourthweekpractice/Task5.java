package fourthweekpractice;

import java.util.Scanner;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */


public class Task5 {
    public static void main(String[] args) {
        Scanner cs = new Scanner(System.in);
        int n = cs.nextInt();
        System.out.println("Входные данные: " + n);
        System.out.println("Выходные данные: ");

        int sum = 0;
        while (n > 0) {

            sum = sum + n % 10;
            System.out.println("Sum is while LOOP: " + sum);
            n = n / 10;
            System.out.println("N in while LOOP " + n);
        }
        System.out.println(sum);


    }
}
// решить эту задачу через Math.abs()