package professionalModul.DZ1;
/*
5
1 3 5 4 5

3
3 2 1

 */
import java.util.Scanner;

public class Dop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int max = 0;
        int max2 = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
            if (array[i] > max2)
            {
                max2 = array[i];
            }
            if (max2 > max)
            {
                int temp = max;
                max = max2;
                max2 = temp;
            }
        }

        System.out.println(max + " " + max2);
    }
}
