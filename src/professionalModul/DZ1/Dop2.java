package professionalModul.DZ1;
/*
Входные:
5
-42 -12 3 5 8
5
Выходные:
3

Входные:
2
17 19
20
Выходные:
-1
 */
import java.util.Scanner;

public class Dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(binarySearch(array,scanner.nextInt()));

    }
    public static int binarySearch(int[] array, int p) {
        int low = 0;
        int high = array.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (p < array[mid]) {
                high = mid - 1;
            } else if (p == array[mid]) {
                return mid;
            } else {
                low = mid + 1;
            }
        } return  - 1;
    }
}
