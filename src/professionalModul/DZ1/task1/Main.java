package professionalModul.DZ1.task1;

import java.util.Scanner;
// Для того что бы поймать исключение, введите число с плавающей точкой.

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое число: ");
        String s = scanner.nextLine();
        try {
            method(s);
        } catch (MyCheckedException e) {
            System.out.println("Main1#main!error " + e.getMessage());
        }

    }
    public static void method(String s){
        try {
            System.out.println(Integer.parseInt(s));
        }
        catch (NumberFormatException e) {
            throw new MyCheckedException(e.getMessage());
        }
        finally {
            System.out.println("Exit program");
        }


    }
}
