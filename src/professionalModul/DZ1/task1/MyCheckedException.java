package professionalModul.DZ1.task1;

public class MyCheckedException extends NumberFormatException{
    public MyCheckedException(){
        System.out.println("Неверный ввод");
    }
    public MyCheckedException(String message){
        super(message);
    }
}
