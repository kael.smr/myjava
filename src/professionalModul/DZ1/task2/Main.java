package professionalModul.DZ1.task2;

import java.util.Scanner;
//Исключение ловится вводом индекса s.length() + 1;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Введите строку: ");
            String s = scanner.nextLine();
            System.out.println("Введите индекс искомого символа: ");
            int n = scanner.nextInt();

            method(s, n);
        }
        catch (MyUncheckedException e){
            System.out.println("Main1#main!error " + e.getMessage());
        }

    }
    public static void method(String s, int n) throws MyUncheckedException{
        try {
            System.out.println(s.charAt(n));
        }
        catch (StringIndexOutOfBoundsException e) {
            throw new MyUncheckedException(e);
        }


    }
}
