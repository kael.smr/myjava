package professionalModul.DZ1.task2;

public class MyUncheckedException extends StringIndexOutOfBoundsException{
    public MyUncheckedException(StringIndexOutOfBoundsException e){
        System.out.println("Введенный индекс вне данной строки");
    }
    public MyUncheckedException(String message){
        super(message);
    }
}
