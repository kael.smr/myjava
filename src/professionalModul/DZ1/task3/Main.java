package professionalModul.DZ1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private  static final String PKG_DIRECTORY = "C:\\Users\\Frai\\IdeaProjects\\FirstPathProject\\src\\PracticeOOPBazis\\week8\\task2\\file";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";


    public static void main(String[] args) {
        try {
            readAndWriteFile();
        }
        catch (IOException e) {
            System.out.println("FileReadWrite#main!error: " + e.getMessage());
        }

    }
    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        ArrayList<String> list = new ArrayList<>();
        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine().toUpperCase());
        }
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (String s : list) {
            writer.write(s + "\n");
        }
        try (scanner; writer) {
            while (scanner.hasNext()) {
                writer.write(scanner.nextLine() + "\n");

            }
        }
    }
}
