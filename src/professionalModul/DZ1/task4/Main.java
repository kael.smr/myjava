package professionalModul.DZ1.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            MyEvenNumber myEvenNumber = new MyEvenNumber(scanner.nextInt());
            if (myEvenNumber.getA() % 2 == 0){
                System.out.println("Успешный ввод");
            }
            else {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Число должно быть четным!");
        }
        finally {
            System.out.println("Программа завершена!");
        }
    }
}
