package professionalModul.DZ1.task5;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        int n = inputN();
    }

    private static int inputN() {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        try {
            if (n > 100 || n < 0) {
                throw new Exception();
            }
            System.out.println("Успешный ввод!");
            return n;
        } catch (Exception e){
            System.out.println("inputN#Main#Error!: Неверный ввод!" );
        }
        finally {
            System.out.println("Программа завершена.");
        }
        return n;
    }
}
