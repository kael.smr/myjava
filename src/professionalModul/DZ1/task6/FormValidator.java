package professionalModul.DZ1.task6;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class FormValidator {
    private String name;
    private String birthDay;
    private String gender;
    private String heist;

    public FormValidator() {

    }


    public String getName() {
        return name;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public String getHeist() {
        return heist;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setHeist(String heist) {
        this.heist = heist;
    }

    public void checkName(String str) {

        if (str.matches("[A-Z|А-Я][a-z|а-я]{1,19}")) {
            setName(str);
        } else {
            System.out.println("Неверный ввод");
        }
    }

    public void checkBirthDay(String str){

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate ld = LocalDate.parse(str,dateTimeFormatter);
        LocalDate ldn = LocalDate.now();
        String lastDate = "01.01.1900";
        String result = "";
        LocalDate ldl = LocalDate.parse(lastDate,dateTimeFormatter);
        if (ld.isAfter(ldl) && ld.isBefore(ldn)){
            result = ld.format(dateTimeFormatter);
            setBirthDay(result);

        } else {
            System.out.println("Неверно введена дата");
        }
    }
    public void checkGender(String str){
        if (Gender.FEMALE.toString().equals(str)||Gender.MALE.toString().equals(str)) {
            setGender(str);
        } else {
            System.out.println("Пол выбран некорректно!");
        }

    }
    public void checkHeight(String str){
        double n = Double.parseDouble(str);
        if (n > 0) {
            setHeist(str);
        } else {
            System.out.println("Некорректный ввод роста!");
        }
    }

}

