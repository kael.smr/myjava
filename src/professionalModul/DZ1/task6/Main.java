package professionalModul.DZ1.task6;

import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        try {
        Scanner scanner = new Scanner(System.in);
        FormValidator formValidator = new FormValidator();
            System.out.println("Введите имя, начиная с заглавной буквы: ");
            formValidator.checkName(scanner.next());

            System.out.println("Введите дату рождения в формате дд.мм.гггг: ");
            formValidator.checkBirthDay(scanner.next());

            System.out.println("Введите ваш пол:\nМужской - MALE;\nЖенский - FEMALE.\n");
            formValidator.setGender(scanner.next());

            System.out.println("Введите ваш рост в формате: СМ.ММ");
            formValidator.setHeist(scanner.next());

        }
        catch (Exception e) {
            System.out.println("Неверно введены данные! " + e.getMessage());
        }


    }
}
