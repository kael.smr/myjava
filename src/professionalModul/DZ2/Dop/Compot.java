package professionalModul.DZ2.Dop;

public class Compot {
    private String word;
    private int id;


    public Compot(String word, int id){
        this.word = word;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getWord() {
        return word;
    }
}
