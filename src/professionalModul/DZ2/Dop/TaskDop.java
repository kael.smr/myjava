package professionalModul.DZ2.Dop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/*
Дайте пожалуйста наводку, куда копать, в мою голову пока не приходит идея, как сделать нужную сортировку.
 */
public class TaskDop {
    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};
        int k = 4;
        ArrayList<Compot> compot = new ArrayList<>();

        for (int i = 0; i < k; i++) {
            int result = 0;
            for ( int j = 0; j < words.length; j++) {
                if (words[i].equalsIgnoreCase(words[j])) {
                    result += 1;
                }
            }
            compot.add(new Compot(words[i], result));

        }
        compot.sort(Comparator.comparingInt(Compot::getId).reversed());

        for (Compot value : compot) {
            System.out.println(value.getWord() + " " + value.getId());
        }



    }
}
