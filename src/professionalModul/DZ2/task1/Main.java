package professionalModul.DZ2.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;



public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(2);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(3);
        arrayList.add(5);
        arrayList.add(5);
        arrayList.add(7);
        arrayList.add(9);

        System.out.println(uniqueElements(arrayList));

    }

    public static <T> Set<T> uniqueElements(ArrayList<T> arr) {
        Set<T> set;
        set = new HashSet<>(arr);

        return set;

    }

}
