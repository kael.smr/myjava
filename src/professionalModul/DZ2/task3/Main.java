package professionalModul.DZ2.task3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        for (int i = 1; i < 4; i++) {
            set1.add(i);
            System.out.print(i + " ");
        }

        Set<Integer> set2 = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            if (i == 3)
                continue;
            set2.add(i);
            System.out.print(i + " ");
        }
        PowerfulSet powerfulSet = new PowerfulSet();
        System.out.println(powerfulSet.intersection(set1,set2));
        System.out.println(powerfulSet.union(set1,set2));
        System.out.println(powerfulSet.relativeComplement(set1,set2));
    }
}
