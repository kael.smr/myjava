package professionalModul.DZ2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Main {
    public static void main(String[] args) {
        ArrayList<Document> arrayList = new ArrayList<>();

        organizeDocuments(arrayList);
        for (Map.Entry<Integer, Document> entry : organizeDocuments(arrayList).entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document document : documents) {
            map.put(document.id, document);
        }
        return map;

    }


}
