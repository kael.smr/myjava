package professionalModul.DZ3.task2;


import professionalModul.DZ3.task1.IsLike;

@IsLike(a = true)
public class Main {
    public static void main(String[] args) {
        checkAnnotation(Main.class);


    }
    public static void checkAnnotation(Class<?> cls){
        if (!cls.isAnnotationPresent(IsLike.class)){
            return;
        }
        IsLike isLike = cls.getAnnotation(IsLike.class);
        System.out.println(isLike.a());
    }
}
