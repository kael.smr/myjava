package professionalModul.DZ3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Class<APrinter> aPrinterClass = APrinter.class;
        try {
            Method method = aPrinterClass.getDeclaredMethod("print", int.class);
            method.invoke(new APrinter(), 15.0);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException |
                 IllegalArgumentException  e) {
            System.out.println(e);
        }
    }
}
