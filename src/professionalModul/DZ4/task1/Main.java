package professionalModul.DZ4.task1;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        int sum= Stream
                .iterate(1, n -> ++n)
                .limit(100)
                .filter(o -> o % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println(sum);
    }
}
