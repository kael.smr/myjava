package professionalModul.DZ4.task2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> myPlaces = List.of(1, 2, 3, 4, 5);
        int sum = myPlaces.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
        System.out.println(sum);
    }
}
