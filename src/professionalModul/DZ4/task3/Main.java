package professionalModul.DZ4.task3;


import java.util.List;


public class Main {
    public static void main(String[] args) {
        List<String> myPlaces = List.of("abc", "", "", "def", "qqq");
        long sc=myPlaces.stream()
                .filter(s -> s.length() > 0)
                .count();
        System.out.println(sc);

    }
}
