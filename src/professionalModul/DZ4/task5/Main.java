package professionalModul.DZ4.task5;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> st = List.of("abc", "def", "qqq");
        System.out.println(st.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(",")));
    }
}
