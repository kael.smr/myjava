package professionalModul.DZ4.task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> x = Set.of(Set.of(3, 2, 5), Set.of(0, 1, 4), Set.of(9, 8, 11), Set.of(6, 7, 10));

        Set<Integer> st = x.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        System.out.println(st);


    }

}
