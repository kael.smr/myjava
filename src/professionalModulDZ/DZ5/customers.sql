create table customers
(
id bigserial primary key,
name_of_customer varchar(50) not null ,
phone_number varchar(100) not null
)

insert into customers(name_of_customer, phone_number)
values ('Danila', 89279828741);
insert into customers(name_of_customer, phone_number)
values ('Kate', 89191128441);
insert into customers(name_of_customer, phone_number)
values ('Alex', 89997788454);
insert into customers(name_of_customer, phone_number)
values ('Sveta', 89517766554);

select * from customers;