create table orders
(
id bigserial primary key,
product_id integer REFERENCES product (id),
customers_id integer REFERENCES customers (id),
quantity integer check (quantity > 0 and quantity < 1001),
amount integer  not null,
date_add date not null
);

select * from orders;
insert into orders(product_id,customers_id,quantity,amount, date_add)
values (1, 2, 10, 1000, now() - interval '4 day')
insert into orders(product_id,customers_id,quantity,amount, date_add)
values (3, 4, 1000, 25000, now() - interval '5 day')

select * from orders join customers on orders.customers_id = customers."id" join product on orders.product_id = product."id"
where orders.id = 2;

select  * from  orders where date_add between now() - interval '1month' and now() and customers_id = 1;

select p.label, o.quantity as Количество from orders o inner join product p on p.id = o.product_id
where "quantity" = (select max("quantity") from orders);

select sum(amount) from orders;