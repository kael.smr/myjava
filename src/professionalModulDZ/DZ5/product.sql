create table product
(
id bigserial primary key,
label varchar(50) not null ,
price integer not null
);
insert into product(label, price)
values('Роза', 100);
insert into product(label, price)
values('Лилия', 50);
insert into product(label, price)
values('Ромашка', 25);

select * from product;