package secondweekpractice;

import java.util.Scanner;

/*
Дано число n. Нужно проверить четное ли оно.
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str;
//        if (n % 2 == 0) {
//            str = "Число " + n + " четное!";
//        }
//        else {
//            str = "Число " + n + " не четное!";
//        }
        // применение тернарного оператора как альтернатива if-else
        str = ( n % 2 == 0 ) ? "Число " + n + " четное!" : "Число " + n + " нечетное!";
        System.out.println(str);
    }
}
