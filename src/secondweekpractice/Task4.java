package secondweekpractice;

/*
Считать данные из консоли о типе номера отеля.
1 Vip 2 Premium 3 Standard
Вывести цену номера Vip = 125, Premium = 110, Standard = 100
 */

import java.util.Scanner;

public class Task4 {
    public static final int VIP_PRISE = 125;
    public static final int PREMIUM_PRISE = 110;
    public static final int STANDARD_PRISE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите тип номера, что бы узнать цену: \n1 - VIP\n2 - Premium\n3 - Standard");
        int number = scanner.nextInt();
        // Стандартное решение через if
//        if (number == 1) {
//            System.out.println("VIP Prise " + VIP_PRISE);
//        }
//        if (number == 2) {
//            System.out.println("Premium Prise " + PREMIUM_PRISE);
//        }
//        if (number == 3) {
//            System.out.println("Standard Prise " + STANDARD_PRISE);
//        }
//        else
//            System.out.println("Доступно только три опции");
        // Стандартная версия switch (поддерживается любой версией JDK)
        switch (number) {
            case 1:
                System.out.println("VIP Prise " + VIP_PRISE);
                break;
            case 2:
                System.out.println("Premium Prise " + PREMIUM_PRISE);
                break;
            case 3:
                System.out.println("Standard Prise " + STANDARD_PRISE);
                break;
            default:
                System.out.println("Доступно только три опции");
                break;
        // новый формат switch поддерживается в JDK 17+
//        switch (number) {
//            case 1 -> System.out.println("VIP Prise " + VIP_PRISE);
//            case 2 -> System.out.println("Premium Prise " + PREMIUM_PRISE);
//            case 3 -> System.out.println("Standard Prise " + STANDARD_PRISE);
//            default -> System.out.println("Доступно только три опции");


        }
    }
}
