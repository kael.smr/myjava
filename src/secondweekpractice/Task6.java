package secondweekpractice;

import java.util.Scanner;

/*
Даны 3 числа a, b, c.
Найти сумму двух чисел, больших по значению из них.
Входные данные
21 0 8
Выходные данные
29
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        // мой вариант решения
//        if (a > c && b > c) {
//            System.out.println(a + b);
//        }
//        if (a > b && c > b) {
//            System.out.println(a + c);
//        }
//        else {
//            System.out.println(b + c);
//        }
        // решение педагога
//        int sum;
//        if (a <= b && a <= c) {
//            sum = b + c;
//        } else if (b <= a && b <= c) {
//            sum = a + c;
//        }
//        else {
//            sum = b + a;
//        }
//        System.out.println(sum);
        //Самый короткий вариант решения используя двойной Math.max
        System.out.println(Math.max(Math.max(a + b, b + c), a + c));
    }
}
