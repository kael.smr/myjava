package secondweekpractice;
/*
Реализовать функуцию System.out.println(), System.out.print() и перенос \n
Входные данные: два слова, считываются из консоли

Входные данные
Hello World
Выходные данные
Hello
World
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstWord = scanner.next();
        String secondWord = scanner.next();
        System.out.print(firstWord + "\n" + secondWord);
    }
}
