package secondweekpractice;

import java.util.Scanner;

/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */
public class Task8 {
    public static void main(String[] args) {
        //моя запись задачи
//        int res;
//        Scanner scanner = new Scanner(System.in);
//        char a = scanner.next().charAt(0);
        //мое первое решение
//        if ((int)a < 91) {
//            res = a + 32;
//        }
//        else {
//            res = a - 32;
//        }
        // мое второе решение
//        res = (int) a < 91 ?  a + 32 : a - 32;
//        System.out.println((char) res);
        // решение препода
        Scanner scanner = new Scanner(System.in);
        String symbol = scanner.next();
        char c = symbol.charAt(0);

        if (c >= 'a' && c <= 'z') {
            System.out.println((char) (c + ('A' - 'a')));
        } else {
            System.out.println((char) (c - ('A' - 'a')));
        }
    }
}
